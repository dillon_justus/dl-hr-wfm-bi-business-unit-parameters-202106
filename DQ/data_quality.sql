INSERT INTO dqf.dqf_control(solution_key,solution_name,solution_job,solution_type,solution_ifs,status,table_type)
VALUES ('hr/wfm/bi/business_unit_parameters','hr_wfm_bi_business_unit_parameters','hr_wfm_bi_business_unit_parameters','inbound','IFS000','ACTIVE','M');

INSERT INTO dqf.dqf_config (solution_job,metric_id,source,validated,vault,column_name,table_validated,table_vault)
VALUES ('hr_wfm_bi_business_unit_parameters','COUNT','true','true','true','','hr_wfm_bi_business_unit_parameters','sat_hr_wfm_bi_business_unit_parameters');



INSERT INTO dqf.dqf_config (solution_job,metric_id,source,validated,vault,column_name,table_validated,table_vault)
VALUES ('hr_wfm_bi_business_unit_parameters','NULL','true','true','true','business_unit_id','hr_wfm_bi_business_unit_parameters','sat_hr_wfm_bi_business_unit_parameters');
