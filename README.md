# DATALAKE DEV PACK #

Dev Pack for DataLake Loads.

### Usage

* Create Repo On Bitbicket  (Projects Name)
* Clone to Local
* Clone This Repo (Dev Pack)
* Copy the folder to the new local Repo Folder
* Add Preject to DEVTAC (link your Repo)
* Open Studio and choose you project

### ToDo

*  Update Talend Job Context
*  Update Artifact file
*  Update SRF
*  Update Scripts
*  Update Vault Config
*  Update DQ Config


### Checklist to Proceed to QA

* DL Load Run on DEVTAC
* Vault Ran on DEVTAC
* DQ run on DEVTAC
* Proof of unit Tests DL
* Proof of unit Tests VAULT
* Proof of unit Tests DQ
* Technical Specification
